using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Plate : MonoBehaviour, IInteractible
{
    public PlayerBody body;
    public int pointCount = 0;
    [SerializeField] private AudioClip open;
    [SerializeField] private AudioSource audioB;

    public void Interact(PlayerInteractManager playerInteractManager, PlayerBody playerBody)
    {
        Debug.Log(body.isHoldingChoppedLettuce);
        if (body.isHoldingChoppedLettuce)
        {
            pointCount++;
            body.SetHoldingChoppedLettuce(false);
            body.score.AddScore();
            audioB.clip = open;
            audioB.Play();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
