using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractBehaviourSO : ScriptableObject
{
    public abstract void Interact(PlayerInteractManager playerInteractManager, PlayerBody playerBody, GameObject thisObject);
}
