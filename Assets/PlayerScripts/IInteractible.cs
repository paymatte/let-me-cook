using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractible
{
    //this function will execute the functionallity to interact for this particular interactable function
    void Interact(PlayerInteractManager playerInteractManager, PlayerBody playerBody);
}
