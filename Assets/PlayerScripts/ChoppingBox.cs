using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ChoppingBox : MonoBehaviour, IInteractible
{
    public PlayerBody body;
    public bool hasLettuce = false;
    public GameObject lettuce;
    public GameObject choppedLettuce;

    public int chopCounter;
    public int chopStart = 5;
    [SerializeField] private AudioClip chop;
    [SerializeField] private AudioClip swing;

    [SerializeField] private AudioClip grab;
    [SerializeField] private AudioSource audioB;


    private void Start()
    {
        lettuce.SetActive(false);
        choppedLettuce.SetActive(false);
    }

    public void Interact(PlayerInteractManager playerInteractManager, PlayerBody playerBody)
    {
        if (body.isHoldingLettuce && !hasLettuce)
        {
            hasLettuce = true;
            body.SetHoldingLettuce(false);
            chopCounter = chopStart;
            lettuce.SetActive(true);
            audioB.clip = grab;
            audioB.Play();
        }   
        else if(chopCounter <= 0 && !body.isHoldingLettuce && !body.isHoldingChoppedLettuce && !body.isHoldingKnife)
        {
            choppedLettuce.SetActive(false);
            hasLettuce = false;
            body.SetHoldingChoppedLettuce(true);
            audioB.clip = grab;
            audioB.Play();
        }

    }

    public void Chop()
    {
        Debug.Log("isChopping");

        if (hasLettuce)
        {
            audioB.clip = chop;
            audioB.Play();
            chopCounter--;
            Debug.Log(chopCounter + "chopping");
            if(chopCounter<= 0)
            {
                choppedLettuce.SetActive(true);
                lettuce.SetActive(false);
            }
        }
        else
        {
            audioB.clip = swing;
            audioB.Play();
        }
    }

}
