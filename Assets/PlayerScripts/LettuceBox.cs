using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LettuceBox : MonoBehaviour, IInteractible
{
    public bool isHoldingLettuce = false;
    public PlayerBody body;

    [SerializeField] private AudioClip open;
    [SerializeField] private AudioSource audioB;


    public void Interact(PlayerInteractManager playerInteractManager, PlayerBody playerBody)
    {
        isHoldingLettuce = true;
        body.SetHoldingLettuce(isHoldingLettuce);
        if(body.isHoldingLettuce)
        {
            audioB.clip = open;
            audioB.Play();
        }



    }

}
