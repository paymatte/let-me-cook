using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trashcan : MonoBehaviour, IInteractible
{
    public PlayerBody body;
    [SerializeField] private AudioClip open;
    [SerializeField] private AudioSource audioB;

    public void Interact(PlayerInteractManager playerInteractManager, PlayerBody playerBody)
    {
        if(body.isHoldingChoppedLettuce || body.isHoldingLettuce)
        {

            audioB.clip = open;
            audioB.Play();
        }
        body.SetHoldingChoppedLettuce(false);
        body.SetHoldingLettuce(false);


    }

}
