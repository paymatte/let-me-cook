using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class CarryBoxInteraction : MonoBehaviour, IInteractible
{
    [SerializeField] private Transform boxPos;
    [SerializeField] private Transform player;
    [SerializeField] private Rigidbody boxRigidbody;
    [SerializeField] private float power = 10f;
    private bool isHeld;

    public void Interact(PlayerInteractManager playerInteractManager, PlayerBody playerBody)
    {
        isHeld = !isHeld;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isHeld)
        {
            transform.position = boxPos.position;
            transform.rotation = player.rotation;
            boxRigidbody.velocity = Vector3.zero;
        }
    }

    public void Throw()
    {
        if (isHeld)
        {
            isHeld = false;

            Vector3 forwardV = transform.position - player.position;
            forwardV.Normalize();
            forwardV = forwardV + Vector3.up;
            forwardV.Normalize();
            boxRigidbody.AddForce(forwardV * power * boxRigidbody.mass, ForceMode.Impulse);
        }
    }
}
