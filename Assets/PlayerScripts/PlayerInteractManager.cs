using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerInteractManager : MonoBehaviour
{
        #region Variable
        List<GameObject> interactableObjects = new List<GameObject>();
        [SerializeField] private PlayerBody playerBody;
        [SerializeField] private PlayerController playerController;
        private bool isHoldingBox = false;
        private GameObject heldBox;
       // ChoppingBox c = new ChoppingBox();

        public UnityEvent onInteractablesExists;
        public UnityEvent onInteractablesDoNotExist;
        #endregion
        private void OnTriggerEnter(Collider other)
        {
       // Debug.Log("intereact ");
            if (other.CompareTag("Interactable"))
            {
                TrackObject(other.gameObject);
            }
        }
        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Interactable"))
            {
                StopTrackingObject(other.gameObject);
            }
        }
        public void Interact()
        {
            if (interactableObjects.Count == 0)
            {
                return;
            }
            IInteractible objectToInteract = interactableObjects[0].GetComponent<IInteractible>();
            objectToInteract.Interact(this, playerBody);
        }
        #region organization
        private void TrackObject(GameObject objectToTrack)
        {
        
            //CarryBoxInteraction c = objectToTrack.GetComponent<CarryBoxInteraction>();
            if (objectToTrack.TryGetComponent<ChoppingBox>(out ChoppingBox cBox))
            {
                playerBody.SetChoppingBox(cBox);
                heldBox = objectToTrack;
                interactableObjects.Add(objectToTrack);

            }
            else
            {
                interactableObjects.Add(objectToTrack);
            }

        if (interactableObjects.Count == 1)
            {
                onInteractablesExists.Invoke();
            }
        }

        public void StopTrackingObject(GameObject objectToTrack)
        {
            if (interactableObjects.Contains(objectToTrack))
            {
                interactableObjects.Remove(objectToTrack);
                if (objectToTrack.Equals(heldBox))
                {
                    heldBox = null;
                    playerBody.SetChoppingBox(null);
                    //playerController.RemoveBox();
                    //playerController.box = null;
                }
                

            if (interactableObjects.Count == 0)
                {
                }
            }

        }

   /* public void Throw()
    {
        if (isHoldingBox)
        {
            c.Throw();
        }
    }*/
        #endregion
    }