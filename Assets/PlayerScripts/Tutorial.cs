using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    public Canvas TutoralCanvas;
    public Canvas hudCanvas;
    public TimerScript timerScript;
    public ThrowFood f;


    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0f;

        hudCanvas.gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void tutorialComplete()
    {
        Time.timeScale = 1f;

        TutoralCanvas.gameObject.SetActive(false);
        hudCanvas.gameObject.SetActive(true);
        timerScript.timerStart = true;
        f.gameStarted = true;
    }
}
