using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;

public class ThrowFood : MonoBehaviour
{
    public GameObject[] prefabs; //Prefabs to spawn
    public AudioClip[] sounds;
    private List<GameObject> spawnedObjects = new List<GameObject>();
    private int maxObjects = 20;

    Camera c;
    int selectedPrefab = 0;
    int rayDistance = 300;
    public bool gameStarted = false;


    //Force
    public float thrust = 20f;
    // Start is called before the first frame update
    void Start()
    {
        c = GetComponent<Camera>();
        if (prefabs.Length == 0)
        {
            Debug.LogError("You haven't assigned any Prefabs to spawn");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (gameStarted)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                selectedPrefab++;
                if (selectedPrefab >= prefabs.Length)
                {
                    selectedPrefab = 0;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                selectedPrefab--;
                if (selectedPrefab < 0)
                {
                    selectedPrefab = prefabs.Length - 1;
                }
            }

            if (Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.LeftShift))
            {
                //Remove spawned prefab when holding left shift and left clicking
                Transform selectedTransform = GetObjectOnClick();
                if (selectedTransform)
                {
                    Destroy(selectedTransform.gameObject);
                }
            }
            else if (Input.GetMouseButtonDown(0))
            {
                //On left click spawn selected prefab and align its rotation to a surface normal
                Vector3[] spawnData = GetClickPositionAndNormal();

                if (spawnData[0] != Vector3.zero)
                {
                    Vector3 coord = new Vector3(spawnData[0].x, spawnData[0].y + 3, spawnData[0].z - 15);

                    int index = Random.Range(0, 10);

                    //GameObject go = Instantiate(prefabs[selectedPrefab], spawnData[0], Quaternion.FromToRotation(prefabs[selectedPrefab].transform.up, spawnData[1]));
                    GameObject go = Instantiate(prefabs[index], coord, Quaternion.FromToRotation(prefabs[selectedPrefab].transform.up, spawnData[1]));
                    go.name += " _instantiated";
                    go.transform.localScale = new Vector3(30.0f, 30.0f, 30.0f); //Scaling

                    //go.AddComponent<Rigidbody>();
                    //go.GetComponent<Rigidbody>().AddForce(transform.right * thrust);
                    Rigidbody projectileRb = go.AddComponent<Rigidbody>();
                    projectileRb.AddForce(thrust * Vector3.forward, ForceMode.Impulse);
                    projectileRb.mass = Random.Range(300, 500);
                    SphereCollider sphereCollider = go.AddComponent<SphereCollider>();

                    //Adding Collision Sounds
                    AudioSource audioSource = go.AddComponent<AudioSource>();

                    int soundIndex = Random.Range(0, sounds.Length);
                    audioSource.clip = sounds[soundIndex];
                    audioSource.playOnAwake = false;

                    // Add your custom script for playing sound on collision
                    PlaySoundOnCollision playSoundScript = go.AddComponent<PlaySoundOnCollision>();


                    //Adds newly created object to the tracking list
                    spawnedObjects.Add(go);

                    //Destorys first element if max is reached
                    if (spawnedObjects.Count > maxObjects)
                    {
                        Destroy(spawnedObjects[0]);
                        spawnedObjects.RemoveAt(0);
                    }
                }
            }
        }
    }

    Vector3[] GetClickPositionAndNormal()
    {
        Vector3[] returnData = new Vector3[] { Vector3.zero, Vector3.zero }; //0 = spawn poisiton, 1 = surface normal
        Ray ray = c.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(ray, out hit, rayDistance))
        {
            returnData[0] = hit.point;
            returnData[1] = hit.normal;
        }

        return returnData;
    }

    Transform GetObjectOnClick()
    {
        Ray ray = c.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(ray, out hit, rayDistance))
        {
            Transform root = hit.transform.root;
            if (root.name.EndsWith("_instantiated"))
            {
                return root;
            }
        }

        return null;
    }
}
