using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Credits : MonoBehaviour
{
    public Button b;
    private bool enabledBool = false;
    // Start is called before the first frame update
    void Start()
    {
        b.gameObject.SetActive(enabledBool);
    }

    public void activateCredits()
    {
        enabledBool = !enabledBool;
        b.gameObject.SetActive(enabledBool);
    }


}
