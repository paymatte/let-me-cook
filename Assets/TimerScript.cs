using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour
{
    public TMP_Text text;
    public int timeTotal;
    private float timeElapsed = 0f;
    private int timeElaspedSeconds = 0;
    public bool timerStart = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (timerStart)
        {
            timeElapsed += Time.deltaTime;
            timeElaspedSeconds = (int)(timeElapsed % 60);
            int timeRemaining = timeTotal - timeElaspedSeconds;



            int minutesRemaining = timeRemaining / 60;
            timeRemaining -= (minutesRemaining * 60);

            text.text = "Time Remaining: " + minutesRemaining.ToString() + ":" + timeRemaining.ToString();
            if (timeRemaining == 0 && minutesRemaining == 0)
            {
                SceneManager.LoadScene("WinScene");
            }
        }

    }
}
