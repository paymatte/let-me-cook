using System.Collections;
using System.Collections.Generic;
using UnityEditor.ShaderGraph.Internal;
using UnityEngine;

public class ThrowFoodBehavior : MonoBehaviour
{
    public GameObject[] prefabs; //Prefabs to spawn

    Camera c;
    int selectedPrefab = 0;
    int rayDistance = 300;
    public bool gameStarted = false;

    //Force
    
    public float thrust = 20f;

    // Start is called before the first frame update
    void Start()
    {
        c = GetComponent<Camera>();
        if (prefabs.Length == 0)
        {
            Debug.LogError("You haven't assigned any Prefabs to spawn");
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        if (gameStarted)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                selectedPrefab++;
                if (selectedPrefab >= prefabs.Length)
                {
                    selectedPrefab = 0;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                selectedPrefab--;
                if (selectedPrefab < 0)
                {
                    selectedPrefab = prefabs.Length - 1;
                }
            }

            if (Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.LeftShift))
            {
                //Remove spawned prefab when holding left shift and left clicking
                Transform selectedTransform = GetObjectOnClick();
                if (selectedTransform)
                {
                    Destroy(selectedTransform.gameObject);
                }
            }
            else if (Input.GetMouseButtonDown(0))
            {
                //On left click spawn selected prefab and align its rotation to a surface normal
                Vector3[] spawnData = GetClickPositionAndNormal();
                //Debug.Log(spawnData.Length);
                //Debug.Log(spawnData[0]);
                //Debug.Log(spawnData[1]);
                if (spawnData[0] != Vector3.zero)
                {
                    Vector3 coord = new Vector3(spawnData[0].x, spawnData[0].y + 5, spawnData[0].z - 15);


                    //GameObject go = Instantiate(prefabs[selectedPrefab], spawnData[0], Quaternion.FromToRotation(prefabs[selectedPrefab].transform.up, spawnData[1]));
                    GameObject go = Instantiate(prefabs[selectedPrefab], coord, Quaternion.FromToRotation(prefabs[selectedPrefab].transform.up, spawnData[1]));
                    go.name += " _instantiated";

                    //go.AddComponent<Rigidbody>();
                    //go.GetComponent<Rigidbody>().AddForce(transform.right * thrust);
                    Rigidbody projectileRb = go.AddComponent<Rigidbody>();
                    projectileRb.AddForce(thrust * Vector3.forward, ForceMode.Impulse);

                }
            }
        }
    }

    Vector3[] GetClickPositionAndNormal()
    {
        Vector3[] returnData = new Vector3[] { Vector3.zero, Vector3.zero }; //0 = spawn poisiton, 1 = surface normal
        Ray ray = c.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(ray, out hit, rayDistance))
        {
            returnData[0] = hit.point;
            returnData[1] = hit.normal;
        }

        return returnData;
    }

    Transform GetObjectOnClick()
    {
        Ray ray = c.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(ray, out hit, rayDistance))
        {
            Transform root = hit.transform.root;
            if (root.name.EndsWith("_instantiated"))
            {
                return root;
            }
        }

        return null;
    }

    void OnGUI()
    {
        if (prefabs.Length > 0 && selectedPrefab >= 0 && selectedPrefab < prefabs.Length)
        {
            string prefabName = prefabs[selectedPrefab].name;
            GUI.color = new Color(0, 0, 0, 0.84f);
            GUI.Label(new Rect(5 + 1, 5 + 1, 200, 25), prefabName);
            GUI.color = Color.green;
            GUI.Label(new Rect(5, 5, 200, 25), prefabName);
        }
    }
}
