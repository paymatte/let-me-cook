using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FinalScore : MonoBehaviour
{
    public Score s;
    public TMP_Text t;
    // Start is called before the first frame update
    void Start()
    {
        t.text = "Your Score: " + s.playerScore;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
