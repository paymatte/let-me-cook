using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusic : MonoBehaviour
{
    public AudioSource start;
    public AudioSource loop;
    public AudioClip loopingmusic;
    private bool playingLoopingMusic = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!start.isPlaying && !playingLoopingMusic)
        {
            Debug.Log("startplaying");
            loop.clip = loopingmusic;
            loop.Play();
            playingLoopingMusic = true;
        }
        loop.loop = true;

    }
}
