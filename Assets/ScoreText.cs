using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class ScoreText : MonoBehaviour
{
    public TMP_Text text;
    public Score scoreScript;

    // Start is called before the first frame update
    void Start()
    {
        text.text = "Score: " + scoreScript.playerScore;
    }

    // Update is called once per frame
    void Update()
    {
        text.text = "Score: " + scoreScript.playerScore;

    }
}
