using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeHolder : MonoBehaviour, IInteractible
{
    public PlayerBody body;
    private bool isHoldingKnife = false;

    [SerializeField] private AudioClip open;
    [SerializeField] private AudioSource audioB;

    public void Interact(PlayerInteractManager playerInteractManager, PlayerBody playerBody)
    {
        if(!body.isHoldingChoppedLettuce && !body.isHoldingLettuce)
        {
            isHoldingKnife = !isHoldingKnife;
            body.SetHoldingKnife(isHoldingKnife);

            audioB.clip = open;
            audioB.Play();
        }

    }

}
