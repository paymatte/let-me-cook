using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{

    public int playerScore = 0;
    public static Score _instance = null;

    private void Awake()
    {
        #region Singleton

        // If an instance of the GameManager
        // doesn't already exist
        if (_instance == null)
        {
            // Make this object the one
            // that _instance points to
            _instance = this;

            // Though this isn't always necessary
            // for a singleton object, if we want
            // our object to persist between scenes,
            // we can use DontDestroyOnLoad()
            DontDestroyOnLoad(this);
        }
        // Otherwise if an instance already
        // exists and it's not this GameManager
        else if (_instance != this)
        {
            // Destroy this GameManager
            Destroy(gameObject);
        }

        #endregion
    }
        // Start is called before the first frame update
        void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddScore()
    {
        playerScore++;
    }
}
